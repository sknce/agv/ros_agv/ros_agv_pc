#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/header.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/imu.hpp"

#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_msgs/msg/tf_message.hpp"

// #include "imu_filter_madgwick/"


using std::placeholders::_1;
using namespace std::chrono_literals;

float velocity_x = 0.0;
float acc_x = 0.0;
float velocity_y = 0.0;
float acc_y = 0.0;
float velocity_rotation = 0.0;

float position_x = 0.0;
float position_y = 0.0;
float orientation_z = 0.0;

std_msgs::msg::Header current_header;

union float2bytes{
    uint8_t byte_array [4];
    float num;
};

// uint8_t current_message_bytes [19] = {0};

std::vector<unsigned char> current_message_bytes (19);
class OdoMaker : public rclcpp::Node
{
    public:
    OdoMaker():Node("odom_maker") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<sensor_msgs::msg::Imu>("oak/imu/data", 10, std::bind(&OdoMaker::oak_recv_callback, this, _1));

        //creating publisher
        odom_publisher_= this->create_publisher<nav_msgs::msg::Odometry>("odom", 10);
        tf_publisher_= this->create_publisher<tf2_msgs::msg::TFMessage>("tf", 10);
        timer_ = this->create_wall_timer(50ms, std::bind(&OdoMaker::odom_send_callback, this));

    }
    private:
    void oak_recv_callback(const sensor_msgs::msg::Imu & msg) const{

        current_header = msg.header;
        velocity_rotation = std::trunc(msg.angular_velocity.x*100)/100.0;
        acc_x = std::trunc(msg.linear_acceleration.z);
        acc_y = std::trunc(msg.linear_acceleration.y);
        
    }

    void odom_send_callback(){

            // std::cout << std::endl;
            auto message = nav_msgs::msg::Odometry();
            message.child_frame_id = "camera_frame";
            message.header = current_header;
            message.header.frame_id = "odom";

            message.twist.twist.linear.x = velocity_x;
            message.twist.twist.linear.y = velocity_y;
            message.twist.twist.linear.z = 0.0;

            message.twist.twist.angular.x = 0.0;
            message.twist.twist.angular.y = 0.0;
            message.twist.twist.angular.z = velocity_rotation;

            
            orientation_z = orientation_z + 0.05*velocity_rotation;

            velocity_x = velocity_x + 0.05*acc_x;
            velocity_y = velocity_y + 0.05*acc_y;

            position_x = position_x + 0.05*velocity_x;
            position_y = position_y + 0.05*velocity_y;
            

            tf2::Quaternion q_new;
            q_new.setRPY(0.0,0.0,orientation_z);

            message.pose.pose.orientation = tf2::toMsg(q_new);

            message.pose.pose.position.x = position_x;
            message.pose.pose.position.y = position_y;
            message.pose.pose.position.z = 0.0;

            odom_publisher_->publish(message);

            auto message_tf = geometry_msgs::msg::TransformStamped();
            message_tf.header = current_header;
            message_tf.header.frame_id = "odom";
            message_tf.child_frame_id = "camera_frame";
            auto message_tf2 = tf2_msgs::msg::TFMessage();

            std::vector<geometry_msgs::msg::TransformStamped> odom_tf = {message_tf};
            // odom_tf[0] = message_tf;
            message_tf2.transforms = odom_tf;
            tf_publisher_->publish(message_tf2);

    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr odom_publisher_;
    rclcpp::Publisher<tf2_msgs::msg::TFMessage>::SharedPtr tf_publisher_;
    size_t count_;
    rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OdoMaker>());
    rclcpp::shutdown();

  return 0;
}
