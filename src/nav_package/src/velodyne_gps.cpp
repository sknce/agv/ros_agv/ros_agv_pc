#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/header.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/imu.hpp"

#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_msgs/msg/tf_message.hpp"


#include <stdlib.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

#define PORT 8308 
#define MAXLINE 554

// #include "imu_filter_madgwick/"


using std::placeholders::_1;
using namespace std::chrono_literals;

int sockfd; 
char buffer[MAXLINE]; 

struct sockaddr_in     servaddr; 

// uint8_t current_message_bytes [19] = {0};

class VelodyneGPS : public rclcpp::Node
{
    public:
    VelodyneGPS():Node("velodyne_gps") , count_(0){

        //creating publisher
        gps_publisher_= this->create_publisher<std_msgs::msg::String>("odom", 10);
        timer_ = this->create_wall_timer(50ms, std::bind(&VelodyneGPS::process_GPS, this));

    }
    private:
    void process_GPS(){

        int n;
        socklen_t len; 

        n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
            MSG_WAITALL, (struct sockaddr *) &servaddr, 
            &len); 
        buffer[n] = '\0'; 
        RCLCPP_INFO(this->get_logger(), "Dupa" );

    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr gps_publisher_;
    size_t count_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;
    // Creating socket file descriptor 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 

    memset(&servaddr, 0, sizeof(servaddr)); 
        
    // Filling server information 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_port = htons(PORT); 
    servaddr.sin_addr.s_addr = INADDR_ANY; 
            int n;
        socklen_t len; 

        n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
            MSG_WAITALL, (struct sockaddr *) &servaddr, 
            &len); 
        buffer[n] = '\0'; 
        // RCLCPP_INFO(this->get_logger(), buffer );

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<VelodyneGPS>());
    rclcpp::shutdown();

  return 0;
}
