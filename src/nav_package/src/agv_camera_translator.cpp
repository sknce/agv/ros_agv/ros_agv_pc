#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/header.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/imu.hpp"

#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_msgs/msg/tf_message.hpp"



using std::placeholders::_1;
using namespace std::chrono_literals;

std::vector<unsigned char> current_message_bytes (19);
class RawMaker : public rclcpp::Node
{
    public:
    RawMaker():Node("imu_trans") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<sensor_msgs::msg::Imu>("oak/imu/data", 10, std::bind(&RawMaker::oak_recv_callback, this, _1));

        //creating publisher
        imu_raw_publisher_= this->create_publisher<sensor_msgs::msg::Imu>("imu/data_raw", 10);

    }
    private:
    void oak_recv_callback(const sensor_msgs::msg::Imu & msg) const{

        auto message_out = sensor_msgs::msg::Imu();
        message_out = msg;
        message_out.linear_acceleration.x = msg.linear_acceleration.z;
        message_out.linear_acceleration.z = -msg.linear_acceleration.x;

        message_out.angular_velocity.x = msg.angular_velocity.z;
        message_out.angular_velocity.z = -msg.angular_velocity.x;
        imu_raw_publisher_->publish(message_out);
        
    }

    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr imu_raw_publisher_;
    size_t count_;
    rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<RawMaker>());
    rclcpp::shutdown();

  return 0;
}
