#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/header.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/imu.hpp"

#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_msgs/msg/tf_message.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

float w_fl = 0.0;
float w_fr = 0.0;
float w_rl = 0.0;
float w_rr = 0.0;

float velocity_xp = 0.0;
float velocity_yp = 0.0;
float velocity_rotation = 0.0;

float velocity_x = 0.0;
float velocity_y = 0.0;
float phi = 0.0;

float position_x = 0.0;
float position_y = 0.0;
float orientation_z = 0.0;

sensor_msgs::msg::Imu current_imu_data;

union float2bytes{
    uint8_t byte_array [4];
    float num;
};

// uint8_t current_message_bytes [19] = {0};

std::vector<unsigned char> current_message_bytes (19);
class OdoMaker : public rclcpp::Node
{
    public:
    OdoMaker():Node("odom_maker") , count_(0){
        //creating subscriber and callback
        param_subscription_ = this->create_subscription<interfaces::msg::Robotparam>("robot_parameters", 10, std::bind(&OdoMaker::params_recv_callback, this, _1));
        imu_subscription_ = this->create_subscription<sensor_msgs::msg::Imu>("imu/data", 10, std::bind(&OdoMaker::imu_recv_callback, this, _1));


        //creating publisher
        publisher_= this->create_publisher<nav_msgs::msg::Odometry>("odom", 10);
        tf_publisher_= this->create_publisher<tf2_msgs::msg::TFMessage>("tf", 10);
        timer_ = this->create_wall_timer(10ms, std::bind(&OdoMaker::odom_send_callback, this));

    }
    private:
    void params_recv_callback(const interfaces::msg::Robotparam & msg) const{

        w_fl = msg.motor_b.velocity*6.2831/3.0;
        w_fr = -msg.motor_c.velocity*6.2831/3.0;
        w_rl = msg.motor_a.velocity*6.2831/3.0;
        w_rr = -msg.motor_d.velocity*6.2831/3.0;

    }

    void imu_recv_callback(const sensor_msgs::msg::Imu & msg) const{

        current_imu_data = msg;

    }

    void odom_send_callback(){

            // std::cout << std::endl;
            auto message = nav_msgs::msg::Odometry();
            message.header = current_imu_data.header;
            message.child_frame_id = "base_link";
            message.header.frame_id = "odom";

            velocity_xp = (w_fl+w_fr+w_rl+w_rr)*0.1/4.0;
            velocity_yp = (-w_fl+w_fr+w_rl-w_rr)*0.1/4.0;
            velocity_rotation = (-w_fl+w_fr-w_rl+w_rr)*(0.1/(4*(0.673+0.678)));

            tf2::Quaternion quat_tf;
            tf2::convert(current_imu_data.orientation , quat_tf);
            tf2::Matrix3x3 m(quat_tf);
            double roll, pitch, yaw;
            m.getRPY(roll, pitch, yaw);
            phi = yaw;

            velocity_x = cos(yaw)*velocity_xp + sin(yaw)*velocity_yp;
            velocity_y = sin(yaw)*velocity_xp + cos(yaw)*velocity_yp;

            message.twist.twist.linear.x = velocity_x;
            message.twist.twist.linear.y = velocity_y;
            message.twist.twist.linear.z = 0.0;

            message.twist.twist.angular.x = 0.0;
            message.twist.twist.angular.y = 0.0;
            message.twist.twist.angular.z = current_imu_data.angular_velocity.z;

            position_x = position_x + 0.01*velocity_x;
            position_y = position_y + 0.01*velocity_y;

            message.pose.pose.orientation = current_imu_data.orientation;

            message.pose.pose.position.x = position_x;
            message.pose.pose.position.y = position_y;
            message.pose.pose.position.z = 0.0;

            publisher_->publish(message);

            auto message_tf = geometry_msgs::msg::TransformStamped();
            // message_tf.header = current_header;
            message_tf.header = current_imu_data.header;
            message_tf.header.frame_id = "odom";
            message_tf.child_frame_id = "camera_frame";
            message_tf.transform.rotation = current_imu_data.orientation;
            message_tf.transform.translation.x = message.pose.pose.position.x;
            message_tf.transform.translation.y = message.pose.pose.position.y;
            message_tf.transform.translation.z = message.pose.pose.position.z;
            auto message_tf2 = tf2_msgs::msg::TFMessage();

            std::vector<geometry_msgs::msg::TransformStamped> odom_tf = {message_tf};
            // odom_tf[0] = message_tf;
            message_tf2.transforms = odom_tf;
            tf_publisher_->publish(message_tf2);

    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr publisher_;
    rclcpp::Publisher<tf2_msgs::msg::TFMessage>::SharedPtr tf_publisher_;
    size_t count_;
    rclcpp::Subscription<interfaces::msg::Robotparam>::SharedPtr param_subscription_;
    rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OdoMaker>());
    rclcpp::shutdown();

  return 0;
}
