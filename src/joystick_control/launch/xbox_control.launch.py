import os
import launch
import launch.actions
import launch.substitutions
from launch_ros.actions import Node
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.launch_description_sources import AnyLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():

    return LaunchDescription([

      Node(
            package='joy',
            executable='joy_node',
            name='node_joy',
            namespace=''
      ),

      Node(
            package='joystick_control',
            executable='xbox_steer',
            name='controller',
            namespace=''
      ),

      Node(
            package='amr_control',
            executable='amr_mux',
            name='muxer',
            namespace=''
      ),

    ])