#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/steer.hpp"
#include "interfaces/msg/motorparam.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/bool.hpp"

#include "sensor_msgs/msg/joy.hpp"
#include "geometry_msgs/msg/twist.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

uint8_t robot_msg_type = 0x00;
uint8_t robot_id = 0x10;
uint8_t robot_msg_mode = 0x03;
float robot_msg_nan = std::nanf("");

float x_velocity = 0.0;
float y_velocity = 0.0;
float r_velocity = 0.0;

uint8_t last_triangle = 0;
uint8_t last_panic = 0;
uint8_t last_more_speed = 0;
uint8_t last_less_speed = 0;
uint8_t mode = 0;
uint8_t panic_button = 0;

float robot_velocity = 0.0;

union float2bytes{
    uint8_t byte_array [4];
    float num;
};

// uint8_t current_message_bytes [19] = {0};

std::vector<unsigned char> current_message_bytes (19);
class PS4Controller : public rclcpp::Node
{
    public:
    PS4Controller():Node("ps4_controller") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<sensor_msgs::msg::Joy>("joy", 10, std::bind(&PS4Controller::joy_recv_callback, this, _1));

        //creating publisher
        publisher_steer= this->create_publisher<interfaces::msg::Steer>("msg_bytes", 10);
        publisher_twist= this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
        timer_ = this->create_wall_timer(100ms, std::bind(&PS4Controller::agv_send_callback, this));

    }
    private:
    void joy_recv_callback(const sensor_msgs::msg::Joy & msg) const{
        
        int triangle = msg.buttons[3];
        int more_speed = msg.buttons[9];
        int less_speed = msg.buttons[10];
        int panic = msg.buttons[1];

        if(triangle && !last_triangle)
            mode = !mode;

        if(panic && !last_panic)
            panic_button = !panic_button;

        if(more_speed && !last_more_speed)
            robot_velocity += 0.1;
        
        if(less_speed && !last_less_speed)
            robot_velocity -= 0.1;
        
        RCLCPP_INFO(this->get_logger(), "x:[%f] y:[%f], r:[%f], m:%d",msg.axes[1],msg.axes[0],msg.axes[2] , panic_button);

        float v_x = -msg.axes[1];
        float v_y = -msg.axes[0];
        float v_r = -msg.axes[2];

        last_triangle = triangle;
        last_panic = panic;
        last_more_speed = more_speed;
        last_less_speed = less_speed;

        current_message_bytes[0] = robot_msg_type;
        current_message_bytes[1] = robot_id;

        if(fabs(v_x)<0.07 && fabs(v_y)<0.07 && fabs(v_r)<0.07){
            if(mode)
                robot_msg_mode = 0x03;
            else
                robot_msg_mode = 0x04;
        }else{
            robot_msg_mode = 0x01;
        }

        current_message_bytes[2] = robot_msg_mode;

        if(robot_msg_mode == 0x01){
            float2bytes xv;
            float2bytes yv;
            float2bytes rv;
            v_x = v_x*robot_velocity;
            v_y = v_y*robot_velocity;
            v_r = v_r*robot_velocity/2;
            xv.num = v_x;
            yv.num = v_y;
            rv.num = v_r;
            for(int j = 0; j<4;j++){
                if(fabs(v_x)>0.07)
                    current_message_bytes[3+j] = xv.byte_array[j];
                else
                    current_message_bytes[3+j] = 0x00;

                if(fabs(v_y)>0.07)
                    current_message_bytes[7+j] = yv.byte_array[j];
                else
                    current_message_bytes[7+j] = 0x00;
                    
                if(fabs(v_r)>0.07)
                    current_message_bytes[11+j] = rv.byte_array[j];
                else
                    current_message_bytes[11+j] = 0x00;
            }

            if(fabs(v_x)>0.07)
                x_velocity = v_x;
            else
                x_velocity = 0.0;
            
            if(fabs(v_y)>0.07)
                y_velocity = v_y;
            else
                y_velocity = 0.0;

            if(fabs(v_r)>0.07)
                r_velocity = v_r;
            else
                r_velocity = 0.0;

        }else{
            for (int i = 3; i < 15; i++){
                current_message_bytes[i] = 0x00;
            }
            x_velocity = 0.0;
            y_velocity = 0.0;
            r_velocity = 0.0;
            
        }
        for(int k = 15;k<19;k++){
            current_message_bytes[k] = 0x50;
        }
        
    }

    void agv_send_callback(){

            
            // std::cout << std::endl;
            auto message_steer = interfaces::msg::Steer();
            // RCLCPP_INFO(this->get_logger(), "Publishing: " );
            // std::copy(std::begin(current_message_bytes),std::end(current_message_bytes), std::begin(message.data));
            message_steer.data = current_message_bytes;
            
            // RCLCPP_INFO(this->get_logger(), "Publishing: " );
            // message.data = current_message_bytes;
            publisher_steer->publish(message_steer);

            auto message_twist = geometry_msgs::msg::Twist();

            message_twist.linear.x = x_velocity;
            message_twist.linear.y = y_velocity;
            message_twist.linear.z = 0.0;

            message_twist.angular.x = 0.0;
            message_twist.angular.y = 0.0;
            message_twist.angular.z = r_velocity;

            publisher_twist->publish(message_twist);

    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<interfaces::msg::Steer>::SharedPtr publisher_steer;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_twist;
    size_t count_;
    rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PS4Controller>());
    rclcpp::shutdown();

  return 0;
}
