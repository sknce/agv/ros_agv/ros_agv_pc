#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/steer.hpp"
#include "interfaces/msg/motorparam.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "interfaces/msg/amrsteering.hpp"
#include "std_msgs/msg/string.hpp"

#include "sensor_msgs/msg/joy.hpp"
#include "geometry_msgs/msg/twist.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

uint8_t robot_msg_type = 0x00;
uint8_t robot_id = 0x10;
uint8_t robot_msg_mode = 0x03;
float robot_msg_nan = std::nanf("");

float x_velocity = 0.0;
float y_velocity = 0.0;
float r_velocity = 0.0;

uint8_t last_triangle = 0;
bool last_circle = 0;
uint8_t last_more_speed = 0;
uint8_t last_less_speed = 0;
uint8_t mode = 0;

float x_reading = 0.0;
float y_reading = 0.0;
float r_reading = 0.0;

float robot_velocity = 0.0;

bool flux_brake = true;
bool manual = true;

union float2bytes{
    uint8_t byte_array [4];
    float num;
};

// uint8_t current_message_bytes [19] = {0};

std::vector<unsigned char> current_message_bytes (19);
class PS4Controller : public rclcpp::Node
{
    public:
    PS4Controller():Node("ps4_controller") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<sensor_msgs::msg::Joy>("joy", 10, std::bind(&PS4Controller::joy_recv_callback, this, _1));

        //creating publisher
        // publisher_steer= this->create_publisher<interfaces::msg::Steer>("msg_bytes", 10);
        // publisher_twist= this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
        publisher_amr = this->create_publisher<interfaces::msg::Amrsteering>("amr_control",10);
        timer_ = this->create_wall_timer(100ms, std::bind(&PS4Controller::agv_send_callback, this));

    }
    private:
    void joy_recv_callback(const sensor_msgs::msg::Joy & msg) const{
        
        int triangle = msg.buttons[3];
        int circle = msg.buttons[2];
        int more_speed = msg.buttons[5];
        int less_speed = msg.buttons[4];

        if(triangle && !last_triangle){
            flux_brake = !flux_brake;
        }

        if(circle && !last_circle){
            manual = !manual;
        }

        if(more_speed && !last_more_speed){
            robot_velocity += 0.1;
            if(robot_velocity>2){
                robot_velocity = 2;
            }
        }

        if(less_speed && !last_less_speed){
            robot_velocity -= 0.1;
            if(robot_velocity<0){
                robot_velocity = 0;
            }
        }

        RCLCPP_INFO(this->get_logger(), "x:[%f] y:[%f], r:[%f], m:%d",msg.axes[1],msg.axes[0],msg.axes[2] , mode);

        float v_x = msg.axes[1];
        if(fabs(v_x) >= 0.1)
            x_reading = v_x;
        else
            x_reading = 0.0;

        float v_y = msg.axes[0];
        if(fabs(v_y) >= 0.1)
            y_reading = v_y;
        else
            y_reading = 0.0;

        float v_r = msg.axes[2];
        if(fabs(v_r) >= 0.1)
            r_reading = v_r;
        else
            r_reading = 0.0;

        last_triangle = triangle;
        last_circle = circle;
        last_more_speed = more_speed;
        last_less_speed = less_speed;


    }

    void agv_send_callback(){

            
            // std::cout << std::endl;
            auto message_steer = interfaces::msg::Amrsteering();
            // RCLCPP_INFO(this->get_logger(), "Publishing: " );
            // std::copy(std::begin(current_message_bytes),std::end(current_message_bytes), std::begin(message.data));
            
            
            // RCLCPP_INFO(this->get_logger(), "Publishing: " );
            // message.data = current_message_bytes;
            // publisher_steer->publish(message_steer);

            // auto message_twist = geometry_msgs::msg::Twist();

            // message_twist.linear.x = x_velocity;
            // message_twist.linear.y = y_velocity;
            // message_twist.linear.z = 0.0;

            // message_twist.angular.x = 0.0;
            // message_twist.angular.y = 0.0;
            // message_twist.angular.z = r_velocity;

            // publisher_twist->publish(message_twist);
            message_steer.cmd_vel.linear.x = x_reading;
            message_steer.cmd_vel.linear.y = y_reading;
            message_steer.cmd_vel.angular.z = r_reading;
            message_steer.set_velocity = robot_velocity;
            message_steer.flux_brake = flux_brake;
            message_steer.manual = manual;
            publisher_amr->publish(message_steer);

    }

    rclcpp::TimerBase::SharedPtr timer_;
    // rclcpp::Publisher<interfaces::msg::Steer>::SharedPtr publisher_steer;
    // rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_twist;
    rclcpp::Publisher<interfaces::msg::Amrsteering>::SharedPtr publisher_amr;
    size_t count_;
    rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PS4Controller>());
    rclcpp::shutdown();

  return 0;
}
