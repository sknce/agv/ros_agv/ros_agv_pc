import os
import launch
import launch.actions
import launch.substitutions
from launch_ros.actions import Node
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.launch_description_sources import AnyLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():
    
    config_dir = os.path.join('src/slam_package', 'config')

    return LaunchDescription([
        
      #launch slam toolbox
      IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('slam_toolbox'),
                    'launch',
                    'localization_launch.py'
                ])
            ]),
            launch_arguments={
                'params_file': '/workspaces/ros_agv_pc/src/slam_package/config/mapper_params_online_async.yaml',
                'use_sim_time':'false'
            }.items()
      ),
    ])