import os
import launch
import launch.actions
import launch.substitutions
from launch_ros.actions import Node
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.launch_description_sources import AnyLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():
    
    config_dir = os.path.join('src/slam_package', 'config')

    return LaunchDescription([
        
      #Launch camera
      IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('depthai_ros_driver'),
                    'launch',
                    'camera.launch.py'
                ])
            ]),
      ),

      #Launch lidar
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('rplidar_ros'),
                    'launch',
                    'rplidar_a1_launch.py'
                ])
            ]),
      ),

      #Run stm32 bridge
      Node(
            package='agv_mcu_package',
            executable='stm32_full_bridge',
            name='stm32_bridge',
            namespace=''
      ),

      #Run camera translator
      Node(
            package='nav_package',
            executable='agv_camera_translator',
            name='translator',
            namespace=''
      ),

      #Launch imu filter madgwick
      Node(
            package='imu_filter_madgwick',
            executable='imu_filter_madgwick_node',
            name='imu_filter',
            output='screen',
            parameters=[os.path.join(config_dir, 'imu_filter.yaml')],
      ),

    #    Run agv odometry
    #   Node(
    #         package='nav_package',
    #         executable='agv_odometry',
    #         name='odometry',
    #         namespace=''
    #   ),

        # Node(
        #     package='nav_package',
        #     executable='agv_camera_odometry',
        #     name='odometry',
        #     namespace=''
        # ),

        Node(
            package='nav_package',
            executable='agv_wheel_odometry',
            name='odometry',
            namespace=''
        ),

      #launch robot state publisher
      IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    '/workspaces/ros_agv_pc/install/minimal_model_description/share/minimal_model_description',
                    'launch',
                    'rsp_irl.launch.py'
                ])
            ]),
      ),
    ])