import os
import launch
import launch.actions
import launch.substitutions
from launch_ros.actions import Node
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.launch_description_sources import AnyLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():
    
    config_dir = os.path.join('src/slam_package', 'config')

    return LaunchDescription([

      Node(
            package='rviz2',
            executable='rviz2',
            name='rviz',
            namespace='',
            arguments=['-d', '/workspaces/ros_agv_pc/amr_config.rviz']
      ),

      #launch robot state publisher
      IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    '/workspaces/ros_agv_pc/install/slam_package/share/slam_package',
                    'launch',
                    'slam_localization.launch.py'
                ])
            ]),
      ),
    ])