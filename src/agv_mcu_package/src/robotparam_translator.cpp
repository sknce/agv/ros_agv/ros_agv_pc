#include <cstdio>

#include <stdio.h>
#include <string.h>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/steer.hpp"
#include "interfaces/msg/motorparam.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"


interfaces::msg::Robotparam current_parameters;
bool new_data = false;

using std::placeholders::_1;
using namespace std::chrono_literals;

class ParamTranslator : public rclcpp::Node
{
    public:
    ParamTranslator():Node("ParamTranslator") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<interfaces::msg::Robotparam>("robot_parameters", 10, std::bind(&ParamTranslator::parameter_recv_callback, this, _1));

        //creating publisher
        publisher_a_ = this->create_publisher<interfaces::msg::Motorparam>("motor_parameters_a", 10);
        publisher_b_ = this->create_publisher<interfaces::msg::Motorparam>("motor_parameters_b", 10);
        publisher_c_ = this->create_publisher<interfaces::msg::Motorparam>("motor_parameters_c", 10);
        publisher_d_ = this->create_publisher<interfaces::msg::Motorparam>("motor_parameters_d", 10);
        timer_ = this->create_wall_timer(200ms, std::bind(&ParamTranslator::motor_send_callback, this));

    }
    private:
    void parameter_recv_callback(const interfaces::msg::Robotparam & msg) const{
        new_data = true;
        current_parameters = msg;
    }

    void motor_send_callback(){
        if(new_data){
            auto message_a = interfaces::msg::Motorparam();
            auto message_b = interfaces::msg::Motorparam();
            auto message_c = interfaces::msg::Motorparam();
            auto message_d = interfaces::msg::Motorparam();
            message_a = current_parameters.motor_a;
            message_b = current_parameters.motor_b;
            message_c = current_parameters.motor_c;
            message_d = current_parameters.motor_d;
            // std::cout << std::endl;
            publisher_a_->publish(message_a);
            publisher_b_->publish(message_b);
            publisher_c_->publish(message_c);
            publisher_d_->publish(message_d);
            new_data = false;
        }
    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<interfaces::msg::Motorparam>::SharedPtr publisher_a_;
    rclcpp::Publisher<interfaces::msg::Motorparam>::SharedPtr publisher_b_;
    rclcpp::Publisher<interfaces::msg::Motorparam>::SharedPtr publisher_c_;
    rclcpp::Publisher<interfaces::msg::Motorparam>::SharedPtr publisher_d_;
    size_t count_;
    rclcpp::Subscription<interfaces::msg::Robotparam>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ParamTranslator>());
    rclcpp::shutdown();

  return 0;
}
