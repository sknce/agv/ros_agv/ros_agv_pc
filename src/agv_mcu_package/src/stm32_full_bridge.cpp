#include <cstdio>

#include <stdio.h>
#include <string.h>
// #include <ostringstream>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/steer.hpp"
#include "interfaces/msg/motorparam.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"


int serial_port;

struct motor_parameters{
	float position;
	float velocity;
	float torque;
    float power;
	float voltage;
    float temperature;
	uint8_t mode;
	uint16_t fault;
};

void file_op(const interfaces::msg::Steer & msg){
    std::ostringstream convert;
    for (int a = 0; a < 19; a++) {
        convert << (int)msg.data[a];
    }
    std::string msg_string = convert.str();       
    // printf("I heard"); //this can be removed
    // RCLCPP_INFO(this->get_logger(),"I heard '%s'", msg_string.c_str()); //this can be removed
    uint8_t arr[19];
    //msg is a custom data structure and it reads as a vector. we need to convert it to array to send to STM
    copy(msg.data.begin(),msg.data.end(),arr);

    write(serial_port, arr, 19);
}


using std::placeholders::_1;
using namespace std::chrono_literals;

class STMbridge : public rclcpp::Node
{
    public:
    STMbridge():Node("STM_bridge") , count_(0){
        //creating subscriber and callback
        subscription_ = this->create_subscription<interfaces::msg::Steer>("msg_bytes", 10, std::bind(&STMbridge::stm_send_callback, this, _1));

        //creating publisher
        publisher_ = this->create_publisher<interfaces::msg::Robotparam>("robot_parameters", 10);

        // raw_publisher_ = this->create_publisher<std_msgs::msg::String>("raw_stm_messages", 10);
        timer_ = this->create_wall_timer(200ms, std::bind(&STMbridge::stm_recv_callback, this));

    }
    private:
    void stm_send_callback(const interfaces::msg::Steer & msg) const{
        file_op(msg);
    }

    void stm_recv_callback(){
        //here we should read from stm32 ie from UART
        char read_buf [109] = { 0 };
        uint8_t arr[19] = {0};
        arr[0] = 0x02;
        for(int i = 1; i <= 18;i++)
            arr[i] = 0x50;

        write(serial_port, arr, 19);

        // now we can read buffer
        int n = read(serial_port, &read_buf, sizeof(read_buf));
        if(n == 109){
            motor_parameters motor_1;
            motor_parameters* ptr_motor_1 = &motor_1;
            memcpy(ptr_motor_1,&read_buf[1],27);

            motor_parameters motor_2;
            motor_parameters* ptr_motor_2 = &motor_2;
            memcpy(ptr_motor_2,&read_buf[28],27);

            motor_parameters motor_3;
            motor_parameters* ptr_motor_3 = &motor_3;
            memcpy(ptr_motor_3,&read_buf[55],27);

            motor_parameters motor_4;
            motor_parameters* ptr_motor_4 = &motor_4;
            memcpy(ptr_motor_4,&read_buf[82],27);


            // auto message = std_msgs::msg::String();
            auto message = interfaces::msg::Robotparam();

            message.motor_a.position = motor_1.position;
            message.motor_a.velocity = motor_1.velocity;
            message.motor_a.torque = motor_1.torque;
            message.motor_a.voltage = motor_1.voltage;
            message.motor_a.fault = (read_buf[27]<<8)+read_buf[26];
            message.motor_a.mode = motor_1.mode;
            message.motor_a.power = motor_1.power;
            message.motor_a.board_temperature = motor_1.temperature;

            message.motor_b.position = motor_2.position;
            message.motor_b.velocity = motor_2.velocity;
            message.motor_b.torque = motor_2.torque;
            message.motor_b.voltage = motor_2.voltage;
            message.motor_b.mode = motor_2.mode;
            message.motor_b.fault = (read_buf[54]<<8)+read_buf[53];
            message.motor_b.power = motor_2.power;
            message.motor_b.board_temperature = motor_2.temperature;           

            message.motor_c.position = motor_3.position;
            message.motor_c.velocity = motor_3.velocity;
            message.motor_c.torque = motor_3.torque;
            message.motor_c.voltage = motor_3.voltage;
            message.motor_c.mode = motor_3.mode;
            message.motor_c.fault = (read_buf[81]<<8)+read_buf[80];
            message.motor_c.power = motor_3.power;
            message.motor_c.board_temperature = motor_3.temperature;

            message.motor_d.position = motor_4.position;
            message.motor_d.velocity = motor_4.velocity;
            message.motor_d.torque = motor_4.torque;
            message.motor_d.voltage = motor_4.voltage;
            message.motor_d.mode = motor_4.mode;
            message.motor_d.fault = (read_buf[108]<<8)+read_buf[107];
            message.motor_d.power = motor_4.power;
            message.motor_d.board_temperature = motor_4.temperature;

            RCLCPP_INFO(this->get_logger(), "Got: '%d'",n );


            // std::ostringstream convert;
            // for (int i = 1; i < 109; ++i)
            //     convert << std::hex << std::setfill('0') << std::setw(2) << (int)read_buf[i] << " ";

            // auto raw_message = std_msgs::msg::String();
            // raw_message.data = convert.str();

            // raw_publisher_->publish(raw_message);
            publisher_->publish(message);
        }
    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<interfaces::msg::Robotparam>::SharedPtr publisher_;
    // rclcpp::Publisher<std_msgs::msg::String>::SharedPtr raw_publisher_;
    size_t count_;
    rclcpp::Subscription<interfaces::msg::Steer>::SharedPtr subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;
    //connecting to STM32 via USB
    serial_port = open("/dev/ttyACM0", O_RDWR);
    struct termios tty;

    // Read in existing settings, and handle any error
    if(tcgetattr(serial_port, &tty) != 0) {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));

    }

    // fcntl(serial_port, F_SETOWN, getpid());
    // fcntl(serial_port, F_SETFL, FNDELAY|FASYNC);

    tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
    tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
    tty.c_cflag |= CS8; // 8 bits per byte (most common)
    tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

    tty.c_lflag &= ~ICANON;
    tty.c_lflag &= ~ECHO; // Disable echo
    tty.c_lflag &= ~ECHOE; // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

    tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
    tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
    // tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
    // tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

    tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
    tty.c_cc[VMIN] = 19;

    // Set in/out baud rate to be 115200
    cfsetispeed(&tty, B115200);
    cfsetospeed(&tty, B115200);

    // Save tty settings, also checking for error
    if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
        printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
    }


    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<STMbridge>());
    rclcpp::shutdown();

  return 0;
}
