import rclpy
from rclpy.node import Node

from std_msgs.msg import String
from interfaces.msg import Gps

import socket

UDP_IP = '' #INADDR_ANY
UDP_PORT = 8308

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

class GPSPublisher(Node):

    def __init__(self):
        super().__init__('GPSPublisher')
        self.publisher_ = self.create_publisher(String, 'NMEA', 10)
        self.gps_publisher_ = self.create_publisher(Gps, 'Velo_GPS', 10)
        timer_period = 0.01  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        # while True:
        #     self.timer_callback()

    def timer_callback(self):
        data, addr = sock.recvfrom(554) # buffer size is 554 bytes
        msg = String()
        msg.data = str(data[206:206+72].decode('utf-8'))
        split_data = msg.data.split(",")
        self.publisher_.publish(msg)
        # self.get_logger().info('Publishing: "%s"' % msg.data)
        msg_gps = Gps()
        msg_gps.utc_time = str(split_data[1][0:2]+":"+split_data[1][2:4]+"."+split_data[1][4:6])
        if(split_data[2]=="A"):
            msg_gps.status = "Valid position"
        elif(split_data[2]=="V"):
            msg_gps.status = "NAV receiver warning"
        else:
            msg_gps.status = "Unknown"

        msg_gps.latitude_string = split_data[3]
        msg_gps.latitude_float = float(split_data[3][0:2])+(float(split_data[3][2:])/60)
        msg_gps.latitude_hemisphere = split_data[4]

        msg_gps.longtitude_string = split_data[5]
        msg_gps.longtitude_float = float(split_data[5][0:3])+(float(split_data[5][3:])/60)
        msg_gps.longtitude_hemisphere = split_data[6]

        msg_gps.speed_over_ground = float(split_data[7])
        msg_gps.course_over_ground = float(split_data[8])

        msg_gps.utc_date = str(split_data[9][0:2]+"."+split_data[9][2:4]+".20"+split_data[9][4:6])

        msg_gps.magnetic_variation = float(split_data[10])
        msg_gps.magnetic_variation_direction = split_data[11]

        if(split_data[12][0]=="A"):
            msg_gps.mode = "Autonomous"
        elif(split_data[12][0]=="D"):
            msg_gps.mode = "Differential"
        elif(split_data[12][0]=="E"):
            msg_gps.mode = "Estimated"
        elif(split_data[12][0]=="N"):
            msg_gps.mode = "Data not valid"
        else:
            msg_gps.mode = "Unknown"

        self.gps_publisher_.publish(msg_gps)

def main(args=None):
    rclpy.init(args=args)

    gps_publisher = GPSPublisher()
    sock.bind((UDP_IP, UDP_PORT))

    rclpy.spin(gps_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    gps_publisher.destroy_node()
    rclpy.shutdown()
    sock.close()


if __name__ == '__main__':
    main()