#include <cstdio>

#include <stdio.h>
#include <string.h>
#include <vector>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "interfaces/msg/steer.hpp"
#include "interfaces/msg/amrsteering.hpp"
#include "interfaces/msg/motorparam.hpp"
#include "interfaces/msg/robotparam.hpp"
#include "std_msgs/msg/string.hpp"

#include "sensor_msgs/msg/joy.hpp"
#include "geometry_msgs/msg/twist.hpp"


using std::placeholders::_1;
using namespace std::chrono_literals;

uint8_t robot_msg_type = 0x00;
uint8_t robot_id = 0x10;
uint8_t robot_msg_mode = 0x03;
float robot_msg_nan = std::nanf("");

auto current_amr_msg = interfaces::msg::Amrsteering();
auto current_cmd_msg = geometry_msgs::msg::Twist();


float x_velocity = 0.0;
float y_velocity = 0.0;
float r_velocity = 0.0;

float robot_velocity = 0.0;

union float2bytes{
    uint8_t byte_array [4];
    float num;
};

// uint8_t current_message_bytes [19] = {0};

std::vector<unsigned char> current_message_bytes (19);
class AMRmux : public rclcpp::Node
{
    public:
    AMRmux():Node("amr_mux") , count_(0){
        //creating subscriber and callback
        amr_subscription_ = this->create_subscription<interfaces::msg::Amrsteering>("amr_control", 10, std::bind(&AMRmux::steer_recv_callback, this, _1));
        cmdvel_subscription_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10, std::bind(&AMRmux::cmdvel_recv_callback, this, _1));

        //creating publisher
        publisher_steer= this->create_publisher<interfaces::msg::Steer>("msg_bytes", 10);
        timer_ = this->create_wall_timer(100ms, std::bind(&AMRmux::agv_send_callback, this));

    }
    private:
    void cmdvel_recv_callback(const geometry_msgs::msg::Twist & msg) const{
        current_cmd_msg = msg;
        
    }
    void steer_recv_callback(const interfaces::msg::Amrsteering & msg) const{
        current_amr_msg = msg;
        robot_velocity = msg.set_velocity;

    }

    void agv_send_callback(){

            
        // std::cout << std::endl;
        auto message_steer = interfaces::msg::Steer();
        // RCLCPP_INFO(this->get_logger(), "Publishing: " );
        // std::copy(std::begin(current_message_bytes),std::end(current_message_bytes), std::begin(message.data));
        current_message_bytes[0] = robot_msg_type;
        current_message_bytes[1] = robot_id;

        float v_x,v_y,v_r;
        if(current_amr_msg.manual){
            v_x = current_amr_msg.cmd_vel.linear.x;
            v_y = current_amr_msg.cmd_vel.linear.y;
            v_r = current_amr_msg.cmd_vel.angular.z/2;
        }else{
            v_x = current_cmd_msg.linear.x;
            v_y = current_cmd_msg.linear.y;
            v_r = current_cmd_msg.angular.z/2;
        }
        
        if(fabs(v_x)<0.07 && fabs(v_y)<0.07 && fabs(v_r)<0.07){
            if(current_amr_msg.flux_brake)
                robot_msg_mode = 0x03;
            else
                robot_msg_mode = 0x04;
        }else{
            robot_msg_mode = 0x01;
        }

        current_message_bytes[2] = robot_msg_mode;

        if(robot_msg_mode == 0x01){
            float2bytes xv;
            float2bytes yv;
            float2bytes rv;
            v_x = v_x*robot_velocity;
            v_y = v_y*robot_velocity;
            v_r = v_r*robot_velocity;
            xv.num = v_x;
            yv.num = v_y;
            rv.num = v_r;
            for(int j = 0; j<4;j++){
                if(fabs(v_x)>0.07)
                    current_message_bytes[3+j] = xv.byte_array[j];
                else
                    current_message_bytes[3+j] = 0x00;

                if(fabs(v_y)>0.07)
                    current_message_bytes[7+j] = yv.byte_array[j];
                else
                    current_message_bytes[7+j] = 0x00;
                    
                if(fabs(v_r)>0.07)
                    current_message_bytes[11+j] = rv.byte_array[j];
                else
                    current_message_bytes[11+j] = 0x00;
            }

        }else{
            for (int i = 3; i < 15; i++){
                current_message_bytes[i] = 0x00;
            }
        }
        for(int k = 15;k<19;k++){
            current_message_bytes[k] = 0x50;
        }

        message_steer.data = current_message_bytes;
        publisher_steer->publish(message_steer);

    }

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<interfaces::msg::Steer>::SharedPtr publisher_steer;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_twist;
    size_t count_;
    rclcpp::Subscription<interfaces::msg::Amrsteering>::SharedPtr amr_subscription_;
    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmdvel_subscription_;
};


int main(int argc, char ** argv)
{
    (void) argc;
    (void) argv;

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<AMRmux>());
    rclcpp::shutdown();

  return 0;
}
