ARG ROS_DISTRO=humble

FROM ros:${ROS_DISTRO}-ros-base
# FROM luxonis/depthai-ros:v2.8.1-humble 
# FROM luxonis/depthai-ros:foxy-latest
# FROM dustynv/ros:foxy-ros-base-l4t-r32.7.1
# FROM dustynv/ros:humble-pytorch-l4t-r32.7.1

ARG BUILD_TYPE="RelWithDebInfo"
ARG SIM=0
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y install --no-install-recommends \
    vim \
    nano \
    zsh \
    software-properties-common \
    libusb-1.0-0-dev \
    python3-colcon-common-extensions \
    python3-rosdep \
    build-essential \
    gpiod \
    libasound2-dev \
    ros-${ROS_DISTRO}-rclpy \ 
    ros-${ROS_DISTRO}-rviz2 \
    ros-${ROS_DISTRO}-pcl-ros \
    ros-${ROS_DISTRO}-std-msgs \
    ros-${ROS_DISTRO}-cv-bridge \ 
    ros-${ROS_DISTRO}-image-transport \
    ros-${ROS_DISTRO}-image-transport-plugins \
    ros-${ROS_DISTRO}-rmw-cyclonedds-cpp \
    ros-${ROS_DISTRO}-image-proc \
    ros-${ROS_DISTRO}-rtabmap-slam \ 
    ros-${ROS_DISTRO}-rtabmap \ 
    ros-${ROS_DISTRO}-rtabmap-ros \ 
    ros-${ROS_DISTRO}-depthai-ros \ 
    gstreamer1.0-plugins-bad \
    alsa-utils \
    mpg123 \
    libmpg123-dev \
    unzip \
    ffmpeg \
    git \
    wget \
    htop \
    python3-pip \
    qtbase5-private-dev \
    libopencv-dev \
    kmod \
    kbd \
    ~nros-humble-rqt*

RUN pip install --upgrade pip && \
    pip install pyserial textual keyboard depthai shapely

RUN echo "source /opt/ros/humble/setup.sh" >> /root/.bashrc

RUN apt update && apt upgrade -y && apt install -y \
    ros-${ROS_DISTRO}-foxglove-bridge \
    ros-${ROS_DISTRO}-bond \
    ros-${ROS_DISTRO}-control-msgs \
    ros-${ROS_DISTRO}-sensor-msgs \
    ros-${ROS_DISTRO}-controller-manager-msgs \
    ros-${ROS_DISTRO}-joy \
    ros-${ROS_DISTRO}-image-transport-plugins \
    ros-${ROS_DISTRO}-gazebo-ros-pkgs \ 
    ros-${ROS_DISTRO}-joint-state-publisher-gui \
    ros-${ROS_DISTRO}-joint-state-publisher*

#SLAM Addon
RUN apt update && apt upgrade -y && apt install -y \
    ros-${ROS_DISTRO}-map-msgs \
    ros-${ROS_DISTRO}-nav2-msgs \
    ros-${ROS_DISTRO}-nav2-bringup \
    ros-${ROS_DISTRO}-navigation2 \
    ros-${ROS_DISTRO}-tf2-msgs \
    ros-${ROS_DISTRO}-rplidar-ros \
    ros-${ROS_DISTRO}-imu-tools \
    ros-${ROS_DISTRO}-slam-toolbox \
    ros-${ROS_DISTRO}-robot-localization && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# ===========================================================================================================
RUN curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.bash
RUN chmod +x nodesource_setup.bash
RUN bash -c ./nodesource_setup.bash
RUN apt-get install -y nodejs

RUN apt-get install -y libyaml-cpp-dev libboost-program-options-dev libwebsocketpp-dev \
    libboost-system-dev libboost-dev libssl-dev libcurlpp-dev \
    libasio-dev libcurl4-openssl-dev*

WORKDIR /is_ws
RUN mkdir src && cd src*
RUN git clone https://github.com/eProsima/Integration-Service.git is
RUN git clone https://github.com/eProsima/WebSocket-SH.git
RUN git clone https://github.com/eProsima/ROS2-SH.git
RUN git clone https://github.com/eProsima/FIWARE-SH.git

RUN npm install -g --unsafe-perm \
    node-red \
    node-red-ros2-plugin \
    node-red-dashboard \
    node-red-contrib-ui-joystick \
    node-red-contrib-remote
# ==========================================================================================================
# # Default powerline10k theme, no plugins installed
# RUN sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.5/zsh-in-docker.sh)"
