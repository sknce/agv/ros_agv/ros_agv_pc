# Makefile for building ROS2 AGV project
SHELL := /bin/bash
PROJECT_NAME := ROS2_AGV_PC
BUILD_DIR := build
INSTALL_DIR := install
LOG_DIR := log

# Source setup file
SOURCE_SETUP := . /opt/ros/humble/setup.bash

# Msg packages (need to be built and sourced first)
MSG_PCKG := interfaces

# Targets
.PHONY: all clean

# Default target
all: clean build

# Build target
build:
	@bash ros_machen.bash
	@bash node_machen.bash
	
# Clean target
clean:
	@echo "Cleaning $(PROJECT_NAME) build files..."
	@rm -rf $(BUILD_DIR) $(LOG_DIR) $(INSTALL_DIR)
