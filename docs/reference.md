# ROS_AGV_PC
This is docker container used for developing software for Student's Scientific Association "Control Engineers" project called: Platform AGV
This file attempts to explain how everything works inside this container.

## Basics
This repository consist of just one directory which includes all files neccesary to run and control AGV platform. To properly use it you should use VScode with Remote Development extension. Reopening this file in container should create docker image and after that you should be able to create ROS2 packages easily.
>Everything is WIP here

### What is a Package? What is a Node?
About that you can read very extensively on [ROS docs page](https://docs.ros.org/en/humble/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Nodes/Understanding-ROS2-Nodes.html).
>A package is an organizational unit for your ROS 2 code. If you want to be able to install your code or share it with others, then you’ll need it organized in a package. ~ *ROS2 Humble docks*

Node is a subsection of a package written in C++ or Python generally implementing one function of a package. Package consist of many nodes that together implement core functionality like autonomous navigation for example. Creating packages and nodes is documented to extents in ROS2 docs.

## Zutaten
This container is really extensieve. You can check all of them in Dockerfile. Most important contents:
- ROS2 humble - base package. Additional packages are listed in the Dockerfile
- QT5 - for gui creation
- Foxglove - for local data visualization - not used yet

## Building project

After reopening in container you *should* be able to build everything using included Makefile, but in reality it doesn't work each time. `ros_machen.bash` script explains what is happening during building
``` bash
ros_machen.bash

source /opt/ros/humble/setup.sh
colcon build --packages-select interfaces
source install/setup.bash
colcon build
source install/setup.bash
```
1. `source /opt/ros/humble/setup.sh` sets up colcon to be used inside container
2. `colcon build --packages-select interfaces` builds only **interfaces** package. Interfaces define our custom messages inside ROS
3. `source install/setup.bash` sources what we built (interfaces)
4. `colcon build` builds the rest of the packages with those that depend on **interfaces**
5. `source install/setup.bash` again sources what we built but with all of the packages

After that it again *should* be possible to use ROS inside container. If it still doesn't work simply source install/setup.bash manually.

Next section explains inter-ROS communication and STM32 bridge

## STM32 bridge
STM32 is necessary for CAN FD communication. It acts as an UART<->CAN FD interface and low level safety system for the robot. It can also be connected via SPI or I2C but that needs further development.

### STM32 frame
This is taken directly from tui app that sends UART directly to STM32. Frame always consists of 19 bytes. As of 15.03.2024 there are two **types** of frames - motor and robot frame. Motor frame sends commands to just one motor - usefull for debugging. Robot frame sends steer vector to robot which calculates velocities by inverse kinematics for all of the motors. 
**Robot frame explanation:**
- 0 : type - robot 0x00, motor 0x01
- 1 : ID - ID of target (motor 0x0a etc. or **robot 0x10**)
- 2 : mode - MOVE 0x01, STOP 0x03, BRAKE 0x04 - explained further
- 3-6 : v_x - speed in m/s in direction of x
- 7-10 : v_y - speed in m/s in direction of y
- 11-14 : rot - speed of rotation in whatever (not specified yet but works)
- 14-18 : filler, filled with NOP 0x50

Steer vector is a vector of three 32bit floats. Implementation can be found in `tui_topic.py` in `textual_gui_pkg`.
Modes are taken directly from [moteus BLDC motor drivers](https://github.com/mjbots/moteus/blob/main/docs/reference.md), but they are adapted to whole robot
- **MOVE** - robot moves with speed given in the vector, we can't ignore vector values sent in frame
- **STOP** - robot is free to move which means it doesn't use any power to drive motors. It also doesn't brake and because of that it retains its inertia after releasing the move button. Additionaly STOP mode resets all off the drivers after a fault that can be caused by not completing message transmission in time (message transmission quality is WIP). In this mode we ignore everything after *mode* section of message
- **BRAKE** - has similiar characteristics as stop mode, but uses flux braking when input is released (i.e velocity message is set to 0,0,0). This also recuperates power back to battery. Also BRAKE doesn't reset drivers

With manual steering those modes of operation are achieved in app tui_topic in textual_qui_pkg.

## Textual gui
Textual is a library for creating powerful and user friendly terminal interfaces
![tui](textual.png)
This is a screenshot from `tui_topic.py`. with it you can steer AGV by WASD for movement in XY axis and QE for rotating in Z axis. `Start` starts sending publishing ROS messages to topic, `Stop` stops it. `Brake\No Brake` button determines robot movement after releasing inputs (i.e sending **(0,0,0)** vector ). Remember that sometimes after bad fault you may have to engage `No Brake` mode for few seconds then back to `Brake` for it to work. By pressing `r` you can toggle dark and light mode of the app

## Running
STM32 bridge is part of agv_mcu_package. To run it you type:
```bash
ros2 run agv_mcu_package stm32_bridge
```

You need to be running this node on Jetson for robot to move. On your computer you have to run ```tui_topic```